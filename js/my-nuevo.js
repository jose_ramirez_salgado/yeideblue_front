  class MyNuevo extends Polymer.Element {
    static get is() { return 'my-nuevo'; }
    static get properties() {
      return {
        page: {
          type: String,
          reflectToAttribute: true,
          observer: '_pageChanged',
        },
        routeData: Object,
        subroute: Object,
        // This shouldn't be neccessary, but the Analyzer isn't picking up
        // Polymer.Element#rootPath
        rootPath: String,
        login: {type: Boolean, value :false, notify:true},
        data:{
          type: Object,
          value:{},
          computed: 'datos()'
      }
      };
    }
    datos(){
        var datos=document.querySelector("my-yeide").data;
          console.log(datos);
          return datos;
     }
     nuevo(){
       var altau = this.generaltau();
       var altac = this.generaltac();
       var altad = this.generaltad();
       var altat = this.generaltat();
        this.altausuario(altau);
        this.altacuentas(altac);
        this.altadir(altad);
        this.altatel(altat);
        alert("alta efectuada");
        limpiar();
     }
     limpiar(){
       this.$.numclient.value="";
       this.$.nombre1.value="";
       this.$.nombre2.value="";
       this.$.apellido1.value="";
       this.$.apellido2.value="";
       this.$.fecha_nacimiento.value="";
       this.$.RFC.value="";
       this.$.password.value="";
       this.$.contrato.value="";
       this.$.plastico.value="";
       this.$.tipocar.value="";
       this.$.saldo.value="";
       this.$.calle.value="";
       this.$.colonia.value="";
       this.$.delegacion.value="";
       this.$.estado.value="";
       this.$.tipodom.value="";
       this.$.lada.value="";
       this.$.telefono.value="";
       this.$.tipotel.value="";
     }
     generaltau(){
       var f = new Date();
       var nuevo={};
       nuevo.numclient=this.$.numclient.value;
       nuevo.admin=false;
       nuevo.nombre1=this.$.nombre1.value;
       nuevo.nombre2=this.$.nombre2.value;
       nuevo.apellido1=this.$.apellido1.value;
       nuevo.apellido2=this.$.apellido2.value;
       nuevo.fecha_ingreso=f.getDate() + "/" + (f.getMonth() +1) + "/" + f.getFullYear();
       nuevo.fecha_nacimiento=this.$.numclient.value;
       nuevo.fecha_nacimiento=this.$.fecha_nacimiento.value;
       nuevo.password=this.$.password.value;
       nuevo.RFC=this.$.RFC.value;
       nuevo =JSON.stringify(nuevo);
       return nuevo;
     }
     generaltac(){
       var f = new Date();
       var nuevo={};
       nuevo.numclient=this.$.numclient.value;
       nuevo.Cuenta=[1];
       nuevo.Cuenta[0]={};
       nuevo.Cuenta[0].contrato=this.$.contrato.value;
       nuevo.Cuenta[0].saldo=this.$.saldo.value;
       nuevo.Cuenta[0].plastico=this.$.plastico.value;
       nuevo.Cuenta[0].tipo=this.$.tipocar.value;
       nuevo.fecha_aper=f.getDate() + "/" + (f.getMonth() +1) + "/" + f.getFullYear();
       nuevo =JSON.stringify(nuevo);
       return nuevo;
     }
     generaltad(){
       var f = new Date();
       var nuevo={};
       nuevo.numclient=this.$.numclient.value;
       nuevo.direccion=[1];
       nuevo.direccion[0]={};
       nuevo.direccion[0].calle=this.$.calle.value;
       nuevo.direccion[0].colonia=this.$.colonia.value;
       nuevo.direccion[0].delegacion=this.$.delegacion.value;
       nuevo.direccion[0].estado=this.$.estado.value;
       nuevo.direccion[0].tipo=this.$.tipodom.value;
       nuevo =JSON.stringify(nuevo);
       return nuevo;
     }
     generaltat(){
       var f = new Date();
       var nuevo={};
       nuevo.numclient=this.$.numclient.value;
       nuevo.telefono=[1];
       nuevo.telefono[0]={};
       nuevo.telefono[0].lada=this.$.lada.value;
       nuevo.telefono[0].telefono=this.$.telefono.value;
       nuevo.telefono[0].tipo=this.$.tipotel.value;
       nuevo =JSON.stringify(nuevo);
       return nuevo;
     }

     altausuario(altau){
       var request = new XMLHttpRequest();
                  request.withCredentials = true;
                  request.addEventListener("readystatechange", function () {
                    if (this.readyState === 4) {
                      console.log(this.responseText);
                    }
                  });
                var cadena = " http://localhost:3000/usuario/";
                console.log(cadena);
                console.log(altau);
                request.open("POST", cadena , true);
                request.setRequestHeader("Accept","application/json");
                request.setRequestHeader("content-type", "application/json");
                request.setRequestHeader("cache-control", "no-cache");
                request.send(altau);
                if (request.status==200){
                  console.log('si paso!');
                }else{
                  console.log(request.status);
                }
     }
     altacuentas(altac){
       var request = new XMLHttpRequest();
                  request.withCredentials = true;
                  request.addEventListener("readystatechange", function () {
                    if (this.readyState === 4) {
                      console.log(this.responseText);
                    }
                  });
                var cadena = " http://localhost:3000/cuentas/";
                console.log(cadena);
                console.log(altac);
                request.open("POST", cadena , true);
                request.setRequestHeader("Accept","application/json");
                request.setRequestHeader("content-type", "application/json");
                request.setRequestHeader("cache-control", "no-cache");
                request.send(altac);
                if (request.status==200){
                  console.log('si paso!');
                }else{
                  console.log(request.status);
                }
     }
     altadir(altad){
       var request = new XMLHttpRequest();
                  request.withCredentials = true;
                  request.addEventListener("readystatechange", function () {
                    if (this.readyState === 4) {
                      console.log(this.responseText);
                    }
                  });
                var cadena = " http://localhost:3000/dir/";
                console.log(cadena);
                console.log(altad);
                request.open("POST", cadena , true);
                request.setRequestHeader("Accept","application/json");
                request.setRequestHeader("content-type", "application/json");
                request.setRequestHeader("cache-control", "no-cache");
                request.send(altad);
                if (request.status==200){
                  console.log('si paso!');
                }else{
                  console.log(request.status);
                }
     }
     altatel(altat){
       var request = new XMLHttpRequest();
                  request.withCredentials = true;
                  request.addEventListener("readystatechange", function () {
                    if (this.readyState === 4) {
                      console.log(this.responseText);
                    }
                  });
                var cadena = " http://localhost:3000/tel/";
                console.log(cadena);
                console.log(altat);
                request.open("POST", cadena , true);
                request.setRequestHeader("Accept","application/json");
                request.setRequestHeader("content-type", "application/json");
                request.setRequestHeader("cache-control", "no-cache");
                request.send(altat);
                if (request.status==200){
                  console.log('si paso!');
                }else{
                  console.log(request.status);
                }
     }

  }
  window.customElements.define(MyNuevo.is, MyNuevo);
