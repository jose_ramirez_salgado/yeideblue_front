class MyMovi extends Polymer.Element {
  static get is() { return 'my-movi'; }
  static get properties() {
    return {
      page: {
        type: String,
        reflectToAttribute: true,
        observer: '_pageChanged',
      },
      ctotal: {
        type: Number
      },
      routeData: Object,
      subroute: Object,
      // This shouldn't be neccessary, but the Analyzer isn't picking up
      // Polymer.Element#rootPath
      rootPath: String,
      login: {type: Boolean, value :false, notify:true},
      data:{
        type: Object,
        value:{},
        computed: 'datos()'
    }
    };
  }
  datos(){

      var datos=document.querySelector("my-yeide").data;
        var consulta= this.movi();
        datos.Movimientos=consulta;
        var i=0;

        while(i<datos.Movimientos[0].movimientos.length){
         datos.Movimientos[0].movimientos[i].importeformateado= this.formato(datos.Movimientos[0].movimientos[i].importe);
         datos.Movimientos[0].movimientos[i].enmascarada="************"+datos.Movimientos[0].movimientos[i].contrato.substring(11,15);
         i++;
       }
       var consultagas=this.consultapreciogas();
       datos.premium=consultagas.results[82].premium;
       console.log( consultagas.results[82].premium);
        return datos;
   }
   formato (valor){
var options = {
  eur: {
    style: "currency",
    currency: "EUR",
    minimumFranctionDigits: 0
  },
  ars: {
    style: "currency",
    currency: "ARS",
    minimumFranctionDigits: 0
  },
  mxn: {
    style: "currency",
    currency: "MXN",
    minimumFranctionDigits: 0
  }
}
  var formateado = new Intl.NumberFormat("es-MX", options['mxn']).format(valor,2);
  return formateado

}

   movi() {
           var request = new XMLHttpRequest();
           var cadena = " http://localhost:3000/movi/"+document.querySelector("my-yeide").data[0].numclient;
           console.log(cadena);
           request.open("GET", cadena ,false);
           request.setRequestHeader("Accept","application/json");
           request.send();
           var movi = JSON.parse(request.responseText);
           return movi;
         }



  consultapreciogas() {
          var request = new XMLHttpRequest();
          var cadena = " http://localhost:3000/consultapreciogas/";
          console.log(cadena);
          request.open("GET", cadena ,false);
          request.setRequestHeader("Accept","application/json");
          request.send();
          var consulta = JSON.parse(request.responseText);
          return consulta;
        }

  round(num, decimales = 2) {
       var signo = (num >= 0 ? 1 : -1);
       num = num * signo;
       if (decimales === 0) //con 0 decimales
           return signo * Math.round(num);
       // round(x * 10 ^ decimales)
       num = num.toString().split('e');
       num = Math.round(+(num[0] + 'e' + (num[1] ? (+num[1] + decimales) : decimales)));
       // x * 10 ^ (-decimales)
       num = num.toString().split('e');
       return signo * (num[0] + 'e' + (num[1] ? (+num[1] - decimales) : -decimales));
  }

}

window.customElements.define(MyMovi.is, MyMovi);
