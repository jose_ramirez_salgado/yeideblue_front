class MyDatos extends Polymer.Element {
  static get is() { return 'my-datos'; }
  static get properties() {
    return {
      page: {
        type: String,
        reflectToAttribute: true,
        observer: '_pageChanged',
      },
      routeData: Object,
      subroute: Object,
      // This shouldn't be neccessary, but the Analyzer isn't picking up
      // Polymer.Element#rootPath
      rootPath: String,
      login: {type: Boolean, value :false, notify:true},
      data:{
        type: Object,
        value:{},
        computed: 'datos()'
    }
    };
  }
  datos(){

      var datos=document.querySelector("my-yeide").data;

        datos.tel=this.datiostel();
        datos.dir=this.datiosdir();
        console.log(datos);

        return datos;
   }


   datiostel() {
           var request = new XMLHttpRequest();
           var cadena = " http://localhost:3000/datiostel/"+document.querySelector("my-yeide").data[0].numclient;
           console.log(cadena);
           request.open("GET", cadena ,false);
           request.setRequestHeader("Accept","application/json");
           request.send();
           this.clientes = JSON.parse(request.responseText);
           return this.clientes;
         }
   datiosdir() {
                 var request = new XMLHttpRequest();
                 var cadena = " http://localhost:3000/datiosdir/"+document.querySelector("my-yeide").data[0].numclient;
                 console.log(cadena);
                 request.open("GET", cadena ,false);
                 request.setRequestHeader("Accept","application/json");
                 request.send();
                 this.clientes = JSON.parse(request.responseText);
                 return this.clientes;
               }


  }
window.customElements.define(MyDatos.is, MyDatos);
