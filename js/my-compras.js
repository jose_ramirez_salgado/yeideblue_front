class MyCompras extends Polymer.Element {
  static get is() { return 'my-compras'; }
  static get properties() {
    return {
      page: {
        type: String,
        reflectToAttribute: true,
        observer: '_pageChanged',
      },
      ctotal: {
        type: Number
      },
      routeData: Object,
      subroute: Object,
      // This shouldn't be neccessary, but the Analyzer isn't picking up
      // Polymer.Element#rootPath
      rootPath: String,
      login: {type: Boolean, value :false, notify:true},
      consultagas:{type: Boolean, value :false, notify:true},
      data:{
        type: Object,
        value:{},
        computed: 'datos()'
    }
    };
  }
  datos(){
      var datos=document.querySelector("my-yeide").data;
      var consulta= this.concuenta();
      var consulta2= this.concuenta();
      datos.cuentas=consulta;
      datos.cuentas2=consulta2;
      var i=0;
      console.log(datos.cuentas[0].Cuenta);
      while(i<datos.cuentas[0].Cuenta.length){
       datos.cuentas[0].Cuenta[i].saldoformateado= this.formato(datos.cuentas[0].Cuenta[i].saldo);
       datos.cuentas[0].Cuenta[i].enmascarada="************"+datos.cuentas[0].Cuenta[i].plastico.substring(12,16);
       datos.cuentas[0].Cuenta[i].indice=i;
       i++;
     }
      var consultagas=this.consultapreciogas();
      datos.premium=consultagas.results[82].premium;
      console.log( consultagas.results[82].premium);
      console.log(datos);
      return datos;
   }
   formato (valor){
      var options = {
        eur: {
          style: "currency",
          currency: "EUR",
          minimumFranctionDigits: 0
        },
        ars: {
          style: "currency",
          currency: "ARS",
          minimumFranctionDigits: 0
        },
        mxn: {
          style: "currency",
          currency: "MXN",
          minimumFranctionDigits: 0
        }
      }
      var formateado = new Intl.NumberFormat("es-MX", options['mxn']).format(valor,2);
      return formateado
    }

   concuenta() {
           var request = new XMLHttpRequest();
           var cadena = " http://localhost:3000/concuenta/"+document.querySelector("my-yeide").data[0].numclient;
           console.log(cadena);
           request.open("GET", cadena ,false);
           request.setRequestHeader("Accept","application/json");
           request.send();
           var concuenta = JSON.parse(request.responseText);
           return concuenta;
         }
   consultacompras(){
     var seleccion= this.$.selectcompras.value;
     if(seleccion==0){
       alert("Seleccione articulo del catalago");
     }else{
       this.consultagas=true;
     }
   }
   consultatotal(){
     var calculo = (this.data.premium*1) * (this.$.Canlitros.value*1);
     this.$.total.value=this.round(calculo,2);
     console.log(calculo);
   }
   comprar(){
     if(this.$.total.value>0){
       if (this.$.cuentapago.value>-1){
         if(this.$.total.value<=this.data.cuentas[0].Cuenta[this.$.cuentapago.value].saldo){
           console.log(this.$.cuentapago.value);
           var alta= this.ejecutaOperacione1();
           var modi= this.ejecutaOperacione2();
           var vale= this.ejecutaOperacione3();
          this.altaBDMovi(alta);
          this.modificaSaldo(modi);
          this.altaVales(vale);

           this.$.mensaje.innerHTML="";
           alert("                  ¡Comprar exitosa!. \n Consulta canjeo, para hacer valido tu vale" );
           this.limpiaCampos();
         }else{
            this.$.mensaje.innerHTML="El monto rebasa el saldo de la cuenta";
         }
       }else{
        this.$.mensaje.innerHTML="Elija medio de pago";
       }
     }else{
      this.$.mensaje.innerHTML="Los litros a comprar deben ser mayor a cero";
     }
   }
   altaBDMovi(alta){
     var request = new XMLHttpRequest();
                request.withCredentials = true;
                request.addEventListener("readystatechange", function () {
                  if (this.readyState === 4) {
                    console.log(this.responseText);
                  }
                });
              var cadena = " http://localhost:3000/altamovimiento/"+document.querySelector("my-yeide").data[0].numclient;
              request.open("PUT", cadena , true);
              request.setRequestHeader("Accept","application/json");
              request.setRequestHeader("content-type", "application/json");
              request.setRequestHeader("cache-control", "no-cache");

              request.send(alta);

              if (request.status==200){
                console.log('si paso!');

              }else{
                console.log(request.status);
              }
   }
   altaVales(vale){
     var request = new XMLHttpRequest();
                request.withCredentials = true;
                request.addEventListener("readystatechange", function () {
                  if (this.readyState === 4) {
                    console.log(this.responseText);
                  }
                });
              var cadena = " http://localhost:3000/vales/";
              console.log(cadena);
              console.log(vale);
              request.open("POST", cadena , true);
              request.setRequestHeader("Accept","application/json");
              request.setRequestHeader("content-type", "application/json");
              request.setRequestHeader("cache-control", "no-cache");
              request.send(vale);
              if (request.status==200){
                console.log('si paso!');
              }else{
                console.log(request.status);
              }
   }
   modificaSaldo(modi){
     var request = new XMLHttpRequest();
                request.withCredentials = true;
                request.addEventListener("readystatechange", function () {
                  if (this.readyState === 4) {
                    console.log(this.responseText);
                  }
                });
              var cadena = " http://localhost:3000/actSaldo/"+document.querySelector("my-yeide").data[0].numclient;
              console.log(cadena);
              console.log(modi);
              request.open("PUT", cadena , true);
              request.setRequestHeader("Accept","application/json");
              request.setRequestHeader("content-type", "application/json");
              request.setRequestHeader("cache-control", "no-cache");

              request.send(modi);

              if (request.status==200){
                console.log('si paso!');

              }else{
                console.log(request.status);
              }
   }
   movi() {
           var request = new XMLHttpRequest();
           var cadena = " http://localhost:3000/movi/"+document.querySelector("my-yeide").data[0].numclient;
           console.log(cadena);
           request.open("GET", cadena ,false);
           request.setRequestHeader("Accept","application/json");
           request.send();
           var movi = JSON.parse(request.responseText);
           return movi;
         }
   ejecutaOperacione2(){

     var ind= this.$.cuentapago.value;
     var modi={};
     modi.numclient= this.data.cuentas2[0].numclient;
     modi.Cuenta=this.data.cuentas2[0].Cuenta;
     var saldoActual=modi.Cuenta[this.$.cuentapago.value].saldo-this.$.total.value;
     saldoActual=this.round(saldoActual,2);
     modi.Cuenta[this.$.cuentapago.value].saldo=saldoActual;
     modi =JSON.stringify(modi);
     console.log(modi);
     return modi;
   }
   ejecutaOperacione1(){
     var alta =null;
     var consulta= this.movi();
     var f = new Date();
     this.data.Movimientos=consulta;
     var longitud=consulta[0].movimientos.length;
     console.log(longitud);
     if(longitud==0){
       var ind= this.$.cuentapago.value;
       var nuevo={};
       nuevo.numclient=document.querySelector("my-yeide").data[0].numclient;
       nuevo.movimientos=[1];
       nuevo.movimientos[0]={};
       nuevo.movimientos[0].contrato=this.data.cuentas[0].Cuenta[ind].contrato;
       nuevo.movimientos[0].fecha=f.getDate() + "/" + (f.getMonth() +1) + "/" + f.getFullYear();
       nuevo.movimientos[0].importe= this.$.total.value;
       nuevo.movimientos[0].descripcion="compra de gasolina";
       nuevo.movimientos[0].Tipo="CARGO";
        alta =JSON.stringify(nuevo);
       console.log(alta);
     }else{
        var ind= this.$.cuentapago.value;
        this.data.Movimientos[0].movimientos.push({});
        this.data.Movimientos[0].movimientos[longitud].contrato=this.data.cuentas[0].Cuenta[ind].contrato;
        this.data.Movimientos[0].movimientos[longitud].fecha=f.getDate() + "/" + (f.getMonth() +1) + "/" + f.getFullYear();
        this.data.Movimientos[0].movimientos[longitud].importe= this.$.total.value;
        this.data.Movimientos[0].movimientos[longitud].descripcion="compra de gasolina";
        this.data.Movimientos[0].movimientos[longitud].Tipo="CARGO";
        var nuevo={};
        nuevo.numclient=document.querySelector("my-yeide").data[0].numclient;
        nuevo.movimientos=this.data.Movimientos[0].movimientos;
        alta =JSON.stringify(nuevo);
       console.log(alta);

     }
     return alta;
   }
   ejecutaOperacione3(){
       var vale =null;
       var f = new Date();
       var nuevo={};
       var dia=f.getDate();
       var mes= (f.getMonth() +1);
       var anio=f.getFullYear();
       var hora=f.getHours();
       var minutos= f.getMinutes();
       var segundos=f.getSeconds();
       var codigo1= dia.toString()+mes.toString()+anio.toString()+hora.toString()+minutos.toString()+segundos.toString();
       var codigo2= this.d2h(codigo1);
       nuevo.numclient=document.querySelector("my-yeide").data[0].numclient;
       nuevo.estatus=true;
       nuevo.monto=this.$.total.value;
       nuevo.tipo="Vale de Gasolina";
       nuevo.litros=this.$.Canlitros.value*1;
       nuevo.fecha=f.getDate() + "/" + (f.getMonth() +1) + "/" + f.getFullYear()+ " "+f.getHours()+":"+f.getMinutes()+":"+f.getSeconds();
       nuevo.codigo=codigo2;
        vale =JSON.stringify(nuevo);
       console.log(vale);
     return vale;
   }
   d2h(d) { return (+d).toString(16).toUpperCase(); }
   limpiaCampos(){
      this.$.cuentapago.value=-1;
      this.$.total.value=0;
      this.$.Canlitros.value=0;
   }
   consultapreciogas() {
          var request = new XMLHttpRequest();
          var cadena = " http://localhost:3000/consultapreciogas/";
          console.log(cadena);
          request.open("GET", cadena ,false);
          request.setRequestHeader("Accept","application/json");
          request.send();
          var consulta = JSON.parse(request.responseText);
          return consulta;
        }

  round(num, decimales = 2) {
       var signo = (num >= 0 ? 1 : -1);
       num = num * signo;
       if (decimales === 0) //con 0 decimales
           return signo * Math.round(num);
       // round(x * 10 ^ decimales)
       num = num.toString().split('e');
       num = Math.round(+(num[0] + 'e' + (num[1] ? (+num[1] + decimales) : decimales)));
       // x * 10 ^ (-decimales)
       num = num.toString().split('e');
       return signo * (num[0] + 'e' + (num[1] ? (+num[1] - decimales) : -decimales));
  }

}

window.customElements.define(MyCompras.is, MyCompras);
