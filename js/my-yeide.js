  // Gesture events like tap and track generated from touch will not be
  // preventable, allowing for better scrolling performance.
  Polymer.setPassiveTouchGestures(true);

  class MyYeide extends Polymer.Element {
    static get is() { return 'my-yeide'; }

    static get properties() {
      return {
        page: {
          type: String,
          reflectToAttribute: true,
          observer: '_pageChanged',
        },
        routeData: Object,
        subroute: Object,
        // This shouldn't be neccessary, but the Analyzer isn't picking up
        // Polymer.Element#rootPath
        rootPath: String,
        login: {type: Boolean, value :false, notify:true},
        admin: {type: Boolean, value :false, notify:true},
      };
    }

    static get observers() {
      return [
        '_routePageChanged(routeData.page)',
      ];
    }

    _routePageChanged(page) {
      // If no page was found in the route data, page will be an empty string.
      // Default to 'view1' in that case.
      this.page = page || 'view1';

      // Close a non-persistent drawer when the page & route are changed.

    }

    _pageChanged(page) {
      if(page=='components'){
        page='accliente'
      }
      // Load page import on demand. Show 404 page if fails
      const resolvedPageUrl = this.resolveUrl('my-' + page + '.html');
      Polymer.importHref(
          resolvedPageUrl,
          null,
          this._showPage404.bind(this),
          true);
    }

    _showPage404() {
      this.page = 'view404';
    }
    salir(){
      document.querySelector("my-yeide").login=false;
      document.querySelector("my-yeide").page ='accliente';
    }
    logine(){
      document.querySelector("my-yeide").login=false;
      document.querySelector("my-yeide").page ='accliente';
    }
    mdatos(){
      document.querySelector("my-yeide").page ='datos';
    }
    nuevo(){
      document.querySelector("my-yeide").page ='nuevo';
    }
    movi(){
      document.querySelector("my-yeide").page ='movi';
    }
    vales(){
      document.querySelector("my-yeide").page ='vales';
    }
    cuentas(){
      document.querySelector("my-yeide").page ='cuentas';
    }
    compras(){
      document.querySelector("my-yeide").page ='compras';
    }

  }

  window.customElements.define(MyYeide.is, MyYeide);
