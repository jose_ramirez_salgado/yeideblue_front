  class MyVales extends Polymer.Element {
    static get is() { return 'my-vales'; }
    static get properties() {
      return {
        page: {
          type: String,
          reflectToAttribute: true,
          observer: '_pageChanged',
        },
        bar: function(e){
          var args = e.target.getAttribute('data-args').split(',');
          // now args = ['foo', 'some other value', '2']
          console.log(args);
        },
        routeData: Object,
        subroute: Object,
        // This shouldn't be neccessary, but the Analyzer isn't picking up
        // Polymer.Element#rootPath
        rootPath: String,
        login: {type: Boolean, value :false, notify:true},
        data:{
          type: Object,
          value:{},
          computed: 'datos()'
      }
      };
    }
    datos(){

        var datos=document.querySelector("my-yeide").data;
          var consulta= this.valesConsulta();
          datos.vales=consulta;
          var i=0;
          console.log(datos);
          return datos;
     }
     consultarqr(){
       alert(this.$.seleccionqr.value);
     }

     formato (valor){
  var options = {
    eur: {
      style: "currency",
      currency: "EUR",
      minimumFranctionDigits: 0
    },
    ars: {
      style: "currency",
      currency: "ARS",
      minimumFranctionDigits: 0
    },
    mxn: {
      style: "currency",
      currency: "MXN",
      minimumFranctionDigits: 0
    }
  }
    var formateado = new Intl.NumberFormat("es-MX", options['mxn']).format(valor,2);
    return formateado

}

     valesConsulta() {
             var request = new XMLHttpRequest();
             var cadena = " http://localhost:3000/vales/"+document.querySelector("my-yeide").data[0].numclient;
             console.log(cadena);
             request.open("GET", cadena ,false);
             request.setRequestHeader("Accept","application/json");
             request.send();
             this.clientes = JSON.parse(request.responseText);
             return this.clientes;
           }


  }

  window.customElements.define(MyVales.is, MyVales);
