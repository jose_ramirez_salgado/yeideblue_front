
  class MyCuentas extends Polymer.Element {
    static get is() { return 'my-cuentas'; }
    static get properties() {
      return {
        page: {
          type: String,
          reflectToAttribute: true,
          observer: '_pageChanged',
        },
        routeData: Object,
        subroute: Object,
        // This shouldn't be neccessary, but the Analyzer isn't picking up
        // Polymer.Element#rootPath
        rootPath: String,
        login: {type: Boolean, value :false, notify:true},
        data:{
          type: Object,
          value:{},
          computed: 'datos()'
      }
      };
    }
    datos(){

        var datos=document.querySelector("my-yeide").data;
          var consulta= this.cuentas();
          datos.cuentas=consulta;
          var i=0;
          console.log(datos.cuentas[0].Cuenta);
          while(i<datos.cuentas[0].Cuenta.length){
           datos.cuentas[0].Cuenta[i].saldoformateado= this.formato(datos.cuentas[0].Cuenta[i].saldo);
           datos.cuentas[0].Cuenta[i].enmascarada="************"+datos.cuentas[0].Cuenta[i].plastico.substring(11,15);
           i++;
         }
         console.log(datos);
          return datos;
     }
     formato (valor){
  var options = {
    eur: {
      style: "currency",
      currency: "EUR",
      minimumFranctionDigits: 0
    },
    ars: {
      style: "currency",
      currency: "ARS",
      minimumFranctionDigits: 0
    },
    mxn: {
      style: "currency",
      currency: "MXN",
      minimumFranctionDigits: 0
    }
  }
    var formateado = new Intl.NumberFormat("es-MX", options['mxn']).format(valor,2);
    return formateado

}


     cuentas() {
             var request = new XMLHttpRequest();
             var cadena = " http://localhost:3000/cuentas/"+document.querySelector("my-yeide").data[0].numclient;
             console.log(cadena);
             request.open("GET", cadena ,false);
             request.setRequestHeader("Accept","application/json");
             request.send();
             this.clientes = JSON.parse(request.responseText);
             return this.clientes;
           }


  }

  window.customElements.define(MyCuentas.is, MyCuentas);
