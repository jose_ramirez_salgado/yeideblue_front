var express = require('express'),
  app = express(),
  port = process.env.PORT || 3021;
  var path = require('path');
  app.listen(port);
  app.use(express.static(__dirname+'/build/es6-unbundled'));

console.log('Yeide Blue server started on: ' + port);

app.get("/", function(req, res) {
  res.sendFile("index.html",{root:'.'});
});
